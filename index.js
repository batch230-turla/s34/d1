/*
	Preparations:
	>> npm init -y
	>> npm install express
*/

// Load the expressjs module into our application and saved it in variable called express
const express = require("express");
const port = 4000;

// app is our server
// create an application that uses express and stores it as app
const app = express();

// Middleware (request handlers)
// express.json() is a method which allow us to handle the streaming of data and automatically parse the incoming JSON from our req.body
app.use(express.json());

// mock data
let users = [
		{
			username: "TStark3000",
			email: "starkindustries@mail.com",
			password: "notPeterParker"
		},
		{
			username: "ThorThunder",
			email: "thorStrongestAvenger@mail.com",
			password: "iLoveStormBreaker"
		}

]



app.get("/", (request, response) => {
	response.send("Hello from my first ExpressJSAPI");
	}
)

/*
	Mini Activity: 5 mins

	>> Create a get route in Expressjs which will be able to send a message in the client:

		>> endpoint: /greeting
		>> message: 'Hello from Batch230-surname'

	>> Test in postman
	>> Send your response in hangouts
*/

app.get("/greeting", (request, response) => {
	response.send("Hello from B230-Turla");
	}
)

// retrieval of users in mock database
app.get("/users", (req, res) => {
	res.send(users);
})

app.post("/users" , (request, response) =>{

	let newUser = {
		username: request.body.username,
		email: request.body.email,
		password: request.body.password
	}

	users.push(newUser);
	console.log(users);

	response.send(users);
})

app.put("/users/:index", (request,response) => {

	console.log(request.body);

	console.log(request.params); // index: 1
	let index = parseInt(request.params.index); // '1' >> 1

	users[index].username = request.body.username;
	users[index].password = reqquest.body.password;

	res.send(users[index]); 

})

// Delete the last element in an array
app.delete('/users', (request, respo) => {
	users.pop();
	res.send(users);
});


//-------------

let items = [
		{
			name: "Mjolnir",
			price: 50000,
			isActive: true
		},
		{
			name: "Vibranium Shield",
			price: 70000,
			isActive: true
		}

]

// Activity
/*
	>> Create a new collection in Postman called s34-activity
	>> Save the Postman collection in your s34 folder
*/

// [GET]
// >> Create a new route to get and send items array in the client (GET ALL ITEMS)
// Insert your code here...


app.get("/items", (req, res) => {
	res.send(items);
})

// // [POST]
// // >> Create a new route to create and add a new item object in the items array (CREATE ITEM)
// 	// >> send the updated items array in the client
// 	// >> check if the post method route for our users for reference




app.post("/items", (req, res) =>{

	let newItems = {
		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	}

	items.push(newItems);
	console.log(items);

	res.send(items);
})


// [PUT]
// >> Create a new route which can update the price of a single item in the array (UPDATE ITEM)







app.put("/items/1", (request,response) => {

	console.log(request.body);

	console.log(request.params); // index: 1
	let price = parseInt(request.params.price); // '1' >> 1

	
	items[1].price = request.body.price;
	

	response.send(items[1]); 

})

/*
	>> Pass the index number of the item that you want to update in the request params (include a index number to the URL)
	>> add the price update update in the request body
	>> reassign the new price from our request body
	>> send the updated item to the client
*/


app.listen(port, () => console.log(`Server is running at port ${port}`));
